<?php

namespace Library;

abstract class BaseModel {
    
    /** @var Container */
    protected $container;

    /** @var DB\mPDO */
    protected $db;


    /** @var int */
    protected $id;
    
    public function __construct(Container $container) {
        $this->container = $container;
        $this->db = new DB;
    }
    
    public function find($id) {
        $sql = "SELECT * FROM {$this->getTableName()} WHERE ID = " . $this->db->escape($id);        
        return $this->db->query($sql);
        
    }
    
    public function findP($param,$input) {
        
        
        $sql = "SELECT * FROM `{$this->getTableName()}` WHERE $param = '{$this->db->escape($input)}'" ;   
        return $this->db->query($sql);
    }
    
    public function findAll()
            {
        $sql="SELECT * FROM  {$this->getTableName()}";
     return  $this->db->query($sql);
            }
    
   public function deleteP($param,$input) {
        
        $sql = "DELETE FROM `{$this->getTableName()}` WHERE $param = '{$this->db->escape($input)}'" ;   
       dd($sql);
        return $this->db->query($sql);
    }
            
    public function delete($id) {
        $sql = "DELETE FROM {$this->getTableName()} WHERE ID = " .  $this->db->escape($id);
        
        $this->db->query($sql);
    }
    
    protected function getTableName() {       
        $reflection = new \ReflectionClass($this);
        return strtolower($reflection->getShortName());
    }

        public function getId() {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    
}


class DB
{

       public function createConnection() {
           $config = file_get_contents("library/config.json");
      $config = json_decode($config,true);
$conn_server = $config["db"]["db_server"];
$conn_user   = $config["db"]["db_user"]; 
$conn_pass   = $config["db"]["db_pass"]; 
$database    = $config["db"]["db_database"];
$conn = mysqli_connect($conn_server,$conn_user,$conn_pass,$database);
if ($conn) {
 return $conn;
 
}
    }
    
    public function query($sql, $Inputs = null)
    {
        $sqlA = explode("?",$sql);
        if(count($sqlA) > 1)
        {
            for($i = 0; $i < count($Inputs); $i++)
            {
                $Inputs[$i] = $this->escape($Inputs[$i]);
            }
            for($i = 0;count($sqlA) > $i; $i++)
            {
                if($i > 0)
                    {
                        $sql = $sql . $Inputs[$i - 1] . $sqlA[$i];
                    }
                else
                    {
                        $sql = $sqlA[0];
                    }
            }
        }
        $result1 = $this->createConnection()->query($sql);
         
        $result = new \stdClass();
         if($result1 != null)
         {
         
         if(gettype ($result1) != "boolean")
         {
         $result->rows = mysqli_fetch_all($result1);
         for($i = 0; $i<count($result->rows);$i++)
           {
           for($j = 0; $j<count($result->rows[$i]);$j++)
           {
               $result->rows[$i][$j] = htmlSpecialChars($result->rows[$i][$j], ENT_QUOTES,"utf-8"); 
           }
           }
         if($result->rows)
         {
         $result->row = $result->rows[0];
         }
         $result->num_rows = mysqli_num_rows($result1);
         }
          
         }
 else {
     
     $result = new \stdClass();
     $result->num_rows = 0;
 }
      
         return $result;
       
    }

    public function escape($value) {
        $search = array("\\", "\0", "\n", "\r", "\x1a", "'", '"');
        $replace = array("\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"');
        return str_replace($search, $replace, $value);
    }
    
}


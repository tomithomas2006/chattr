<?php

namespace Library;


require_once __DIR__ . '/../PHPMailer/PHPMailer/OAuth.php';
require_once __DIR__ . '/../PHPMailer/PHPMailer/PHPMailer.php';
require_once __DIR__ . '/../PHPMailer/PHPMailer/SMTP.php';
require_once __DIR__ . '/../PHPMailer/PHPMailer/Exception.php';
require_once __DIR__ . '/../PHPMailer/PHPMailer/POP3.php';

class Container {

    /** @var array */
    private $parameters = [];

    /** @var array */
    private $services = [];

    public function __construct(array $parameters) {
        $this->parameters = $parameters;
    }

    /**
     * @param string $serviceName
     * @return mixed
     * @throws FactoryServiceIsNotDefined
     */
    public function getService($serviceName) {
        if ($this->hasService($serviceName)) {
            return $this->services[$serviceName];
        } else {
            $factoryName = "create" . ucfirst($serviceName);

            if (!method_exists($this, $factoryName)) {
                throw new FactoryServiceIsNotDefined(
                "Tovarna {$factoryName} neni definovana."
                );
            }

            $this->services[$serviceName] = $this->{$factoryName}();
            return $this->services[$serviceName];
        }
    }

    /**
     * @return \Library\DB\mPDO
     */
    public function createConnection() {
        return new \Library\DB\mPDO(
                $this->get('db_host'),
                $this->get('db_user'),
                $this->get('db_pass'),
                $this->get('db_dbname')
        );
    }
    
    /**
     * @return \App\Model\Country
     */
    public function createCountry() {
        return new \App\Model\Country($this);
    }
    
    /**
     * @return \App\Model\Person
     */
    public function createPerson() {
        return new \App\Model\Person($this);
    }
    
    /**
     * @return \App\Model\Movie
     */
    public function createMovie() {
        return new \App\Model\Movie($this);
    }
    
    /**
     * @return \App\Model\Chat
     */
    public function createChat() {
        return new \App\Model\Chat($this);
    }
    
    /**
     * @return \App\Model\EmailVerification
     */
    public function createEVM() {
        return new \App\Model\EmailVerification($this);
    }
    
    public function createFriend() {
        return new \App\Model\Friends($this);
    }
    
    public function createUser() {
        return new \App\Model\Users($this);
    }
    
     public function createLogin() {
        return new \App\Model\Login($this);
    }
    
    public function createPost() {
        return new \App\Model\Posts($this);
    }
    
    public function createLike() {
        return new \App\Model\Like($this);
    }
    
    public function createAU() {
     return new \App\Model\activeusers($this);   
    }
    
    public function createRegister() {
        return new \App\Model\Register($this);  
    }
    
    public function createComment() {
        return new \App\Model\Comments($this);  
    }
    
    public function createGroup() {
        return new \App\Model\Groups($this);  
    }
    
    public function createGroupMembers() {
        return new \App\Model\Groupmembers($this);  
    }
    
    public function createUserToken() {
        return new \App\Model\UserTokens($this);  
    }
    
     public function createGame() {
        return new \App\Model\Game($this);  
    }
    
    
    
    /**
     * @param string $serviceName
     * @return boolean
     */
    public function hasService($serviceName) {
        return isset($this->services[$serviceName]);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key) {
        if ($this->has($key)) {
            return $this->parameters[$key];
        } else {
            return null;
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key) {
        return isset($this->parameters[$key]);
    }

     public function Wsendmail($email,$body,$subject,$WE)
    {
        if($WE == 0)  
        {
            $this->sendmail($email, $body, $subject);
        }
    }
    
    public function sendmail($email,$body,$subject,$FS = False)
    {
        $USRModel = $this->createUser();
        if($USRModel->FindUserByEmail($email)->row[11] == 0 || $FS = True)
        {
           $mail = new \PHPMailer\PHPMailer\PHPMailer();
           $config = file_get_contents("library/config.json");
           $config = json_decode($config,true);
           // Set mailer to use SMTP
           //  $mail->SMTPDebug = 2;                                 // Enable verbose debug output
           $mail->isSMTP();                                      // Set mailer to use SMTP
           $mail->Host = $config["email"]["email_host"];  // Specify main and backup SMTP servers
           $mail->SMTPAuth = true;                               // Enable SMTP authentication
           $mail->Username = $config["email"]["email_adress"];                 // SMTP username
           $mail->Password = $config["email"]["email_password"];                           // SMTP password
           $mail->SMTPSecure = $config["email"]["email_SMTPSecure"];                            // Enable TLS encryption, `ssl` also accepted
           $mail->Port = 465;                                    // TCP port to connect to
           //Recipients
           $mail->setFrom('postmaster@chattr.cz', 'Chattr-Mailer-Bot');
           $mail->addAddress($email);     // Add a recipient
           $mail->CharSet = 'UTF-8';
           // Optional name
           $body = '<meta charset="UTF-8">' . $body;
           //Content
           $mail->isHTML(true);                                  // Set email format to HTML
           $mail->Subject = $subject;
           $mail->Body    = $body /* . "<br><br><h2>Pokud že nechcete aby vám od nás přicházely e-maily napište na náš e-mail<br>( popřípadě stačí odpovědět na tuto zprávu)</h2>"*/;
           //$mail->AltBody = ;
           $mail->send();
        }
    }
}

<?php

namespace Library;

class ControllerNotFoundException extends \Exception {
    
}

class ActionNotFoundException extends \Exception {
    
}

class FactoryServiceIsNotDefined extends \Exception {
    
}

class BadRequest extends \Exception {
    
}

class RecordNotFoundException extends \Exception {
    
}
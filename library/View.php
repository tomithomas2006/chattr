<?php

namespace Library;

class View {
    
    /** @var string */
    protected $file;
    
    /** @var array */
    protected $data;
    
    public function __construct() {
        $this->file = null;
        $this->data = [];
    }
    
    public function render() {
        // kontrola existence contentu
        if (!file_exists("app/view/" . $this->file)) {
            throw new \Exception("Soubor šablony neexistuje.");
        }
        extract($this->data);
        // vlozeni havicky layoutu
        if (!file_exists("app/view/header.phtml")) {
            throw new \Exception("Soubor hlavičky neexistuje.");
        } else {
            include 'app/view/header.phtml';
        }
        
        // vlozeni contentu a dat z controlleru
        
        include "app/view/" . $this->file;
        
        // vlozeni paticky layoutu
        if (!file_exists("app/view/footer.phtml")) {
            throw new \Exception("Soubor patičky neexistuje.");
        } else {
            include 'app/view/footer.phtml';
        }
    }
    
    public function __set($name, $value) {
        if( gettype($value) == "string")
        {
           $value = htmlSpecialChars($value, ENT_QUOTES,"utf-8"); 
       } else if(gettype($value) == "array")
       {
           for($i = 0; $i<count($value);$i++)
           {
               $value[$i] = htmlSpecialChars($value[$i], ENT_QUOTES,"utf-8"); 
           }
       }else if(gettype($value) == "object" && isset($value->row))
      {
           for($i = 0; $i<count($value->rows);$i++)
           {
           for($j = 0; $j<count($value->rows[$i]);$j++)
           {
               $value->rows[$i][$j] = htmlSpecialChars($value->rows[$i][$j], ENT_QUOTES,"utf-8"); 
           }
           }
       }
        $this->data[$name] = $value;
    }
    
    public function __get($name) {
        return $this->data[$name];
    }
    
    public function getFile() {
        return $this->file;
    }

    public function getData() {
        return $this->data;
    }

    public function setFile($file) {
        $this->file = $file;
        return $this;
    }

    public function setData($data) {
        $this->data = $data;
        return $this;
    }

   
    
}

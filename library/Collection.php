<?php

namespace Library;

final class Collection implements \Iterator, \Countable, \ArrayAccess {
    
    /** @var array */
    private $list;
    
    /** @var array */
    private $keys;
    
    private $type;
    
    public function __construct($type) {
        $this->type = $type;
        $this->list = array();
    }
    
    /**
     * Vlozi objekt na konec kolekce
     * @param {$this->type} $obj
     * @throws \InvalidArgumentException
     */
    public function push($obj) {
        if (!($obj instanceof $this->type)) {
            throw new \InvalidArgumentException(
                    "Argument $obj musí být typu {$this->type}");
        }
        $this->list[] = $obj;
    }
    
    /**
     * Odebere prvek z kolekce a vrati ho
     * @param mixed $key
     * @return {$this->type}
     */
    public function pop($key) {
        $poped = $this->list[$key];
        unset($this->list[$key]);
        return $poped;
    }
    
    public function getType() {
        return $this->type;
    }
    
    /*~~~~~~~~~~ Iterator ~~~~~~~~~~*/

    public function current() {
        if (($key = current($this->keys)) !== false) {
            return $this->list[$key];
        } else {
            return false;
        }
    }

    public function key() {
        return current($this->keys);
    }
    
    public function rewind() {
        $this->keys = array_keys($this->list);
        reset($this->keys);
    }

    public function valid() {
        return current($this->keys) !== false;
    }
    
    /*~~~~~~~~~~ Countable ~~~~~~~~~~*/
    
    public function count() {
        return count($this->list);
    }

    /*~~~~~~~~~~ ArrayAccess ~~~~~~~~~~*/
    
    public function next() {
        return next($this->keys);
    }

    public function offsetExists($offset) {
        return isset($this->list[$offset]);
    }

    public function offsetGet($offset) {
        return $this->list[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->list[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->list[$offset]);
    }

}
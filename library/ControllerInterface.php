<?php

namespace Library;

interface ControllerInterface {
    
    public function startup();
    
}

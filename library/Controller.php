<?php

namespace Library;

abstract class Controller implements ControllerInterface {
    
    /** @var Container */
    protected $container;

    /** @var View */
    protected $template;
    
    /** @var UserID */
    public $User_ID;
    
    public function __construct(Container $container) {
        $this->container = $container;
    }
    
    public function startup() { 
        $this->template = new View();
        $UserTokensM =  $this->container->createUserToken();  
        if(isset($_COOKIE["UserToken"]))
        {
            
            /*$this->User_ID */ $UserData = $UserTokensM->findUserFromToken($_COOKIE["UserToken"]);
            if($UserData->num_rows == 1)
            {
               $this->User_ID = $UserTokensM->findUserFromToken($_COOKIE["UserToken"])->row[0];
               $this->template->User_ID = $this->User_ID;
            } 
        }
        //Make User active
        $this->AU();
        if(isset($this->User_ID))
        {
          //Vulnerability protections
          $this->security();
          //Information for Notifications
          $this->template->FN = $this->FN();
        }
    }
    
    public function AU() {
        session_start();
        //create AU model
        $this->UAM = $this->container->createAU();
        //Make user Active( create record or update existing record)
        if($this->UAM->findspecifiedAU(session_id())->num_rows == 1)
        {
        $this->UAM->updateActiveUser(time(),session_id());
        }
        else
        {      
            if(isset($this->User_ID))
            {
        $this->UAM->addActiveUser(session_id(), time(),$this->User_ID);
            }else
            {
                $this->UAM->addActiveUser(session_id(), time(),NULL);
            }
        }
        //delete active user
        $this->UAM->deleteAU();
    }
    
    public function FN() 
    {
        $MChat = $this->container->createChat(); 
        $MFriend = $this->container->createFriend();
        
        $MUser = $this->container->createUser();
        $ntfctn = $MChat->ControlUserMessages($this->User_ID);
        $count = 0;
        $count = $count + $ntfctn->row[0]; 
        if($MFriend->FindUserRequests($this->User_ID)->num_rows != 0)
        {
            if(isset($count))
            {
                $count = $count + $MFriend->FindUserRequests($this->User_ID)->num_rows;
            }
            else
            {
            $count = $MFriend->FindUserRequests($this->User_ID)->num_rows;
            }
        }
        if($count != 0)
        {
            $text = "(" .  $count . ")";
            return $text;
        }
    }
 
    public function security() {
        $UserM = $this->container->createUser();
        $UserData = $UserM->FindUserData($this->User_ID);
        //Cookie protection
        if($UserData->num_rows == 0 || $UserData->row[8] != $_COOKIE["UserAdmin"])
        {
            setcookie("UserAdmin",NULL,time() - 5000);
            setcookie("UserToken",NULL,time() - 5000);
            setcookie("UserData",NULL, time() - 5000);
            setcookie("UserID",NULL,time() - 5000);
            die ("chyba s cookies" );
        }
        //CSRF protection
        if(!empty($_POST))
        {
            if(!isset($_POST["csrf_token"]))
            {
                echo"1";
                http_response_code(500);
                die("CSRF ochrana");
            }else
            {
                dd($_SESSION["csrf_token"]);
                if($_SESSION["csrf_token"] != $_POST["csrf_token"])
                {
                    http_response_code(500);
                    die("CSRF ochrana");
                }else
                {
                    unset($_SESSION["csrf_token"]);
                }
            }
        }
        
    }
    
   
}
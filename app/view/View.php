<?php

namespace App\View;

class View {
    
    /** @var string */
    protected $file;
    
    /** @var array */
    protected $data;
    
    public function __construct() {
        $this->file = null;
        $this->data = [];
    }
    
    public function render() {
        
        if (file_exists($this->file)) {
            extract($this->data);
            
            include $this->file;
        } else {
            throw new \Exception("Soubor šablony neexistuje.");
        }
        
    }
    
    public function __set($name, $value) {
        $this->data[$name] = $value;
    }
    
    public function __get($name) {
        return $this->data[$name];
    }
    
    public function getFile() {
        return $this->file;
    }

    public function getData() {
        return $this->data;
    }

    public function setFile($file) {
        $this->file = $file;
        return $this;
    }

    public function setData($data) {
        $this->data = $data;
        return $this;
    }


    
}

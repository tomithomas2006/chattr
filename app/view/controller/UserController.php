<?php

namespace App\Controller;

class UserController extends \Library\Controller {
   
    
    public function indexAction() {
        $user = $this->container->createUser();
        
        $this->template->setFile("users/index.phtml");
        
        
        
        
         
          
         $this->template->users = $user->FindAll(); ;      
         $this->template->render();    
    }
    
    
    
    
    public function profilesAction($id)
    {
         $this->template->IFA = $this->UAM->findP("UserID",$id)->num_rows;
        $this->template->setFile("users/profiles.phtml");
        $user = $this->container->createUser();
        $UserData = $user->FindUserData($id); 
        $Posts = $this->container->createPost();
        $Friends = $this->container->createFriend();
        $this->template->PostsData = $Posts->findUserPosts($id);
        $this->template->LM = $this->container->createLike();
        $this->template->UserData = $UserData;
        $this->template->user = $user;
        $this->template->Friends = $Friends;
        $this->template->FFriends = $Friends->FindFriends($UserData->row[0]);
        dd($UserData);
        $this->template->render();
    }
    
    public function findAction() {
        if(isset($_POST))
        {
            $this->template->setFile("users/index.phtml");
            $FirstName = $_POST["FirstName"];
            $LastName = $_POST["LastName"];
            $user = $this->container->createUser();
            $this->template->users = $user->FindUserByName($FirstName,$LastName);
            $this->template->render();
        }
    }
    public function administrationAction() {
        if(isset($this->User_ID))
        {
        $this->template->setFile("users/administration.phtml");
        $this->template->render();
        }
    }
    
   
}

<?php



namespace App\Controller;


class ChatController extends \Library\Controller{
   
    public function addgroupmemberAction($id) {
        $this->template->setFile("general/Status.phtml");
        $this->template->Status = "Nejste přihlášen";
        if(isset($id,$this->User_ID))
        {
       $GroupMM =  $this->container->createGroupMembers();
       $this->template->Status = "Již jste v této skupině";
       if($GroupMM->findgroupmember($id, $this->User_ID)->num_rows == 0)
       {
       $GroupMM->addmember($id, $this->User_ID);
       
        $this->template->Status = "Byl jste přidán do skupiny";
       }
        }
        $this->template->render();
    } 
    
    public function creategroupAction() {
        if(isset($this->User_ID,$_POST["groupname"]))
        {
            $GroupMM = $this->container->createGroupMembers();
            header("Content-Type: application/json" );
            if(strlen($_POST["groupname"]) < 100)
            {
                $data = "Skupina s tímto jménem již byla vytvořena";
            $groupname = $_POST["groupname"];
            $GroupM = $this->container->createGroup();
            if($GroupM->findGroup($groupname, $this->User_ID)->num_rows == 0)
            {
                $rand = rand(1,10000000000);
                while($GroupM->find($rand)->num_rows != 0)
                {
                    $rand = rand(1,10000000000);
                }
            $GroupM->createGroup($rand,$groupname,$this->User_ID);
            $GroupMM->addmember($GroupM->findGroup($groupname, $this->User_ID)->row[0],$this->User_ID);
            $data = "Skupina byla vytvořena";
            }
            echo json_encode($data);
            }
        }
    }
    
    public function groupAction($id) {
        if(isset($this->User_ID))
        {
        $this->template->setFile("chat/group.phtml");
        $GroupMM = $this->container->createGroupMembers();
        $GroupM = $this->container->createGroup(); 
        $Friends = $this->container->createFriend();
        $MUser = $this->container->createUser();
        $Chat = $this->container->createChat(); 
        $this->template->Messages = $Chat->renderMessages($this->User_ID,$id);
        $this->template->MUser = $MUser;
        $this->template->ID = $id;
        $this->template->Friends = $Friends->FindFriends($this->User_ID);
        $this->template->MChat = $Chat;
        $this->template->GroupMM = $GroupMM;
        $this->template->GroupM = $GroupM;
        $this->template->render();
        }
        else
        {
            $this->template->setFile("exception/NotLoged.phtml");
            $this->template->render();
        }
    }
    
    public function grouprendermessageAction($id) {
        $data = " ";
        $Chat = $this->container->createChat(); 
        $MChat = $Chat;
        $UserM = $this->container->createUser(); 
        $Messages = $Chat->groupRenderMessages($id);
        if(isset($this->User_ID,$id))
        {
            dd($Messages);
         if($Messages->num_rows != 0)
       {
        foreach ($Messages->rows as $Messages->row)
            
       { 
           
          
          if($Messages->row[2] === $this->User_ID) 
          {
              $data = $data . "<br><div class='message bubble-user'>"
               . htmlspecialchars($Messages->row[1],ENT_QUOTES,"utf-8") . 
                      "<br><h6>"
                     . $Messages->row[6] . " " . $Messages->row[5] 
                     . "</h6></div>" ;
              
               
              
          }
          else
          {
             $user = $UserM->find($Messages->row[2]);
             $data = $data . "<br><div class='message bubble-friend' style='style='border:2px solid #0AE3ED; background:#0AE3ED;overflow-x: hidden;'>" .
              htmlspecialchars($Messages->row[1],ENT_QUOTES,"utf-8") . 
                "<br><h6>"
                     . $Messages->row[6]. " " . $Messages->row[5] 
                     . "</h6>" . $user->row[1] . " " . $user->row[2] ."</div>" ;
             
            
          }
         
          }
       } 
    $translate_table = file_get_contents("library/wordchange.json");
      $translate_table = json_decode($translate_table,true);
          $data = strtr( $data, $translate_table);
          
    
    $data = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%+.~#?&;//=]+)!i', '<a rel="nofollow" href="$1">$1</a>', $data);



        header("Content-Type: application/json" );
     
       echo json_encode($data);
     /*  $this->template->setFile("exception/NotLoged.phtml");
        $this->template->render();*/
        }
    }
    
    public function indexAction($id) {
        if(isset($this->User_ID))
        {
        $this->template->setFile("chat/index.phtml");
        
        $Friends = $this->container->createFriend();
        $MUser = $this->container->createUser();
        $this->template->IFA = $this->UAM->findP("UserID",$id)->num_rows;
      
        $Chat = $this->container->createChat(); 
        $this->template->Messages = $Chat->renderMessages($this->User_ID,$id);
        $this->template->MUser = $MUser;
        $this->template->Friends = $Friends->FindFriends($this->User_ID);
        $this->template->ID = $id;
        $this->template->MChat = $Chat;
        $this->template->render();
        }
        else
        {
            $this->template->setFile("exception/NotLoged.phtml");
            $this->template->render();
        }
    }
    
    public function groupaddmessageAction($id) {
        if(isset($_POST["MSG"],$this->User_ID))
        {
        $MSG = $_POST["MSG"];
        
        $Chat = $this->container->createChat(); 
        $Chat->GroupAddMessage($this->User_ID, $id, $MSG, date("H:i:s"),date("y-m-d"));
        }
    }
    
    public function addmessageAction($id) {
        if(isset($_POST["MSG"],$this->User_ID))
        {
            $FriendsM = $this->container->createFriend(); 
            if($FriendsM->FindRecordById($id,$this->User_ID)->num_rows == 1)
            {
        $MSG = $_POST["MSG"];
        
        $Chat = $this->container->createChat(); 
        $Chat->AddMessage($this->User_ID, $id, $MSG, date("H:i:s"),date("y-m-d"));
            }
        }
    }
    
    public function rendermessageAction($id) {
        $data = " ";
        $Chat = $this->container->createChat(); 
        $MChat = $Chat;
        if(isset($this->User_ID,$id))
        {
        $Messages = $Chat->renderMessages($this->User_ID,$id);
         if($Messages->num_rows != 0)
       {
        foreach ($Messages->rows as $Messages->row)
            
       { 
          
          
          if($Messages->row[2] == $this->User_ID) 
          {
              $data = $data . "<br><div class='message bubble-user'>"
               . htmlspecialchars($Messages->row[1],ENT_QUOTES,"utf-8") . 
                      "<br><h6>"
                     . $Messages->row[6] . " " . $Messages->row[5] 
                     . "</h6>" ;
              if($Messages->row[4] == 1)
              {
                  $data = $data . " <h6> Přečteno</h6>";
              }
                $data = $data . "</div>";
               
              
          }
          else
          {
          
             $data = $data . "<br><div class='message bubble-friend' style='style='border:2px solid #0AE3ED; background:#0AE3ED;overflow-x: hidden;'>" .
              htmlspecialchars($Messages->row[1],ENT_QUOTES,"utf-8") . 
                "<br><h6>"
                     . $Messages->row[6]. " " . $Messages->row[5] 
                     . "</h6></div>" ;
          }
          if($Messages->row[4] == 0 && $Messages->row[3] == $this->User_ID)
          {
          $MChat->ReadingMessage($Messages->row[0]);
          }
          }
       } 
   $translate_table = file_get_contents("library/wordchange.json");
      $translate_table = json_decode($translate_table,true);
          $data = strtr( $data, $translate_table);
          
    
    $data = preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%+.~#?&;//=]+)!i', '<a rel="nofollow" href="$1">$1</a>', $data);



        header("Content-Type: application/json" );
     $data =   $data;
        echo json_encode($data);
        }
    }
    
    public function smileysAction() {
        $this->template->setFile("chat/smileys.phtml");
        $this->template->render();
    }
    
}

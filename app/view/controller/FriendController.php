<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

/**
 * Description of FriendController
 *
 * @author tomit
 */
class FriendController extends \Library\Controller {
    public function friendrequestAction($id)
    {
        
        $MFriends = $this->container->createFriend();
        if($MFriends->FindRecordById($this->User_ID,$id)->num_rows == 0)
            {
         
        $MFriends->AddNewFriendRequest($this->User_ID, $id);
            }
        
            
        
        $this->indexAction();
        
    }
    
    public function indexAction() {
        $MFriend = $this->container->createFriend();
        if(isset($this->User_ID))
        {
        $GroupMM = $this->container->createGroupMembers();
        $Chat = $this->container->createChat(); 
        $RequestsData = $MFriend->FindUserRequests($this->User_ID);
        $FriendsData = $MFriend->FindFriends($this->User_ID);
        $MUser = $this->container->createUser();
        $this->template->MUser = $MUser;
        $this->template->Requests = $RequestsData;
        $this->template->Friends = $FriendsData;
        $this->template->MFriend = $MFriend;
        $this->template->MChat = $Chat;
        $this->template->GroupMM = $GroupMM;
        $this->template->setFile("friends/index.phtml");     
        $this->template->render();
        }
        else
        {
            
            $this->template->setFile("exception/NotLoged.phtml");
            $this->template->render();
        }
        
        
    }
    
    public function acceptrequestAction($id) {
        $Friends = $this->container->createFriend();
        $Friends->AcceptRequest($id);
       
        $this->indexAction();
        
    }
    
    public function declinerequestAction($id)
    {
       $Friends = $this->container->createFriend();
       $Friends->DeclineRequest($id);
       $Status = "návrh na přátelství odmítnut";
       $this->template->Status = $Status;
        $this->template->setFile("friends/Status.phtml");
        $this->template->render();
    }
    
    
}

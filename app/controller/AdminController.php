<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

/**
 * Description of AdminController
 *
 * @author tomit
 */
class AdminController extends \Library\Controller{
    
    
    public function indexAction() {
        //Verifying
        if(isset($_COOKIE["UserAdmin"]) && $_COOKIE["UserAdmin"] == 1)
        {
            if(isset($this->User_ID))
            {
        $this->template->setFile("admin/index.phtml");
        $UserM = $this->container->createUser();
        $this->template->registeredUserCount = $UserM->FindAll()->num_rows;
        $AUserM = $this->container->createAU();
        $AU = $AUserM->findAUsers();
        $this->template->activeUserCount = $AU->num_rows;
        $this->template->AUnames = $AU;
            }
        }
        else
        {
            $this->template->Status = "Nejste Admin";
            $this->template->setFile("general/Status.phtml");
        }
        $this->template->render();   
    }
    
  
    
}

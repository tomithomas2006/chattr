<?php

namespace App\Controller;

class UserController extends \Library\Controller {
    
    public function GetUserMessagesCSVAction()
    {
        if($this->User_ID)
        {
            //header('Content-Encoding: UTF-8');
            header("Content-Type: text/csv");
            header('Content-Disposition: attachment; filename="Vase_zpravy.csv"');
            $data = $this->container->createChat()->findUserMessages($this->User_ID);
            $data = $data->rows;
            $fp = fopen('php://output', 'wb');
            fputcsv($fp, array("ID", "content", "sender ID", "recipient ID", "Readed", "Message sent(Time)", "Message sent(Date)", "E-mail sent", "Notification in browser sent", "GroupID"));
            foreach($data as $ActualData)
            {
                fputcsv($fp,$ActualData);
            }
        }
        
    }
    
    public function BasicHTMLTable($header,$data)
    {
        $html = '<table border="1" cellpadding="1" cellspacing="1"><tbody><tr>';
        foreach($header as $header)
        {
            $html .= "<td>$header</td>";
        }
        $html .= "</tr>";
        foreach($data as $ActualData)
        {
            $html .= "<tr>";
            foreach($ActualData as $ActualData2)
            {
                $html .= "<td>$ActualData2</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody></table>";
        return $html;
    }
    
    public function SendUserInformationsAction()
    {
        //Controls if is set Token with UserID
        if(isset($this->User_ID))
        {
            $html = '<h3>Dokument o vašich aktivitách na Chattru</h3><br>';
            $html .= '<h6>V tabulkách kde hodnoty se rovnají 1/0 je tím myšleno Pravda/Lež</h6>';
            $ActualInformations = $this->container->createUser()->find($this->User_ID)->rows;
            $html .= "<h4>Informace o vašem profilu</h4>";
            $header = array("ID","Jméno","Príjmení","Vaše heslo", "Datum narození", "Profil. obrázek", "E-mail. adresa", "Popis profilu", "Admin polícko", "Vaše zájmy", "Verifikace E-mail", "Chcete E-maily?");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $ActualInformations = $this->container->createAU()->findP("UserID", $this->User_ID)->rows;
            $html .= "<h4>Vaše poslední aktivita</h4>";
            $header = array("Session ID","čas","uživatelské ID");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $html .= "<h4>Vaše zprávy</h4>";
            $html .= "<h4>pokud chcete ziskat i informace o vašich zprávách jděte zpět na stráku s nastavením a klikněte na 'Stáhnout .csv soubor obsahující zprávy'<h4>";
            $ActualInformations = $this->container->createComment()->findP("UserID",$this->User_ID)->rows;
            $html .= "<h4>Vámi přidané komentáře</h4>";
            $header = array("ID komentáře","ID odesílatele komentáře","ID příspěvku", "Obsah");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $ActualInformations = $this->container->createFriend()->FindFriendsWithoutJoin($this->User_ID)->rows;
            $html .= "<h4>Vaši přátelé</h4>";
            $header = array("ID","ID žadatele","ID přijemce", "Přijate");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $GaM = $this->container->createGame();
            $ActualInformations = $GaM->fus($this->User_ID)->rows;
            $html .= "<h4>Vaše sociální sítě ve hře</h4>";
            $header = array("ID","Jméno","Typ", "Datum vytvoření", "ID uživatele", "Uživatelé zaregistrováno","Prestige");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $ActualInformations = $GaM->ft($this->User_ID)->rows;
            $html .= "<h4>Vaše technologie ve hře</h4>";
            $header = array("ID","ID uživatele","Jméno technologie");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $GrM = $this->container->createGroup();
            $ActualInformations = $this->container->createGroup()->findP("UserID", $this->User_ID)->rows;
            $html .= "<h4>Skupiny</h4>";
            $header = array("ID","jméno skupiny","ID uživatele");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $ActualInformations = $this->container->createGroupMembers()->findP("UserID", $this->User_ID)->rows;
            $html .= "<h4>Uživatelé skupiny</h4>";
            $header = array("ID","ID skupiny","ID uživatele");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $ActualInformations = $this->container->createLike()->FindUserLike($this->User_ID)->rows;
            $html .= "<h4>Liky</h4>";
            $header = array("ID příspěvku","ID uživatele");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $ActualInformations = $this->container->createPost()->findWithUserPosts($this->User_ID)->rows;
            $html .= "<h4>Příspěvky</h4>";
            $header = array("ID","Obsah příspěvku","ID Odesílatele","ID profilu na kterém je příspěvek","Datum přidání","Jméno obrázku");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            $html .= "<h6>Pokud je sloupec `Jméno obrázku` prázdný, tak příspěvek nemá obrázek`</h6>";
            $ActualInformations = $this->container->createUserToken()->findP("Token", $_COOKIE["UserToken"])->rows;
            $html .= "<h4>Token(y)</h4>";
            $header = array("Uživatelský Token","ID vlastníka Tokenu");
            $html .= $this->BasicHTMLTable($header, $ActualInformations);
            require_once __DIR__ . '/../../vendor/autoload.php';
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output();    
        }
    }
    
    public function indexAction() {
        //Show site with Users
        $user = $this->container->createUser();
        $this->template->setFile("users/index.phtml");
        $this->template->users = $user->FindAll();   
        $this->template->render();    
    }
    
    public function WEAction($id) {
        //Set OFF OR ON EmailNotifications
        if(isset($this->User_ID))
        {
            $user = $this->container->createUser();
            $user->WantEmails($this->User_ID, $id);
            echo"<meta http-equiv='refresh' content='1;url=/index.php'>";
        } 
    }

    
    public function profilesAction($id)
    {
        //Show site with profiles
        $this->template->IFA = $this->UAM->findP("UserID",$id)->num_rows;
        $this->template->setFile("users/profiles.phtml");
        $user = $this->container->createUser();
        $UserData = $user->FindUserData($id); 
        $Posts = $this->container->createPost();
        $Friends = $this->container->createFriend();
        $this->template->PostsData = $Posts->findUserPosts($id);
        $this->template->LM = $this->container->createLike();
        $this->template->UserData = $UserData;
        $this->template->user = $user;
        $this->template->Friends = $Friends;
        $this->template->FFriends = $Friends->FindFriends($UserData->row[0]);
        dd($UserData);
        $this->template->render();
    }
    
    public function findAction() {
        if(isset($_POST))
        {
            $this->template->setFile("users/index.phtml");
            $FirstName = $_POST["FirstName"];
            $LastName = $_POST["LastName"];
            $user = $this->container->createUser();
            $this->template->users = $user->FindUserByName($FirstName,$LastName);
            $this->template->render();
        }
    }
    
    public function administrationAction() {
        if(isset($this->User_ID))
        {
            $this->template->setFile("users/administration.phtml");
            $this->template->render();
        }else
        {
            http_response_code(500);
            echo("Užvatel není přihlášen");
        }
    }
    
   
}

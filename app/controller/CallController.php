<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

/**
 * Description of CallController
 *
 * @author tomit
 */
class CallController extends \Library\Controller{
    
    public function soundAction($id) {
        if($id && $this->User_ID)
        {
            $this->template->setFile("call/sound.phtml");
            $this->template->render();
        }
        else
        {
            $this->template->setFile("exception/NotLoged.phtml");
            $this->template->render();
        }
    }
    
}

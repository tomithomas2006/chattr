<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of Users
 *
 * @author tomit
 */
class Users extends \Library\BaseModel{
    
    
    public function verify($Email) {
        $sql = "UPDATE `users` SET `Verified` = 1 WHERE `Email` = '" . $this->db->escape($Email) . "' ";
        dd($sql);
          return $this->db->query($sql);
    }
    
    public function FindUserData($id)
    {
    $sql = "SELECT * FROM `users` WHERE `id` = " . $this->db->escape($id);
    return $this->db->query($sql);
    }
    
    public function WantEmails($User_ID,$state) {
         $sql = "UPDATE `users` SET `WEN` = " . $this->db->escape($state) . " WHERE `ID` = " . $this->db->escape($User_ID);
         
    return $this->db->query($sql);
    }
    
    public function FindUserByName($FirstName,$LastName) {
       
         $sql = "SELECT * FROM `users` WHERE `FirstName` LIKE '%" . $this->db->escape($FirstName) . "%' AND `LastName` LIKE '%" . $this->db->escape($LastName) . "%' ";
         
    return $this->db->query($sql);
    }
    
    public function FindUserByEmail($Email) {
    $sql = "SELECT * FROM `users` WHERE `Email` = '" . $this->db->escape($Email) . "' ";
    
    return $this->db->query($sql);
    }
   
    public function DeleteUser($id) {
    $sql = "DELETE FROM `users` WHERE `ID` = "  . $this->db->escape($id) ;
    
    return $this->db->query($sql);
    }
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of Chat
 *
 * @author tomit
 */
class Chat extends \Library\BaseModel{
    
    public function findUserMessages($UserID) {
        $sql = "SELECT * FROM `messages` WHERE `IDFrom` = ? OR `IDTo` = ?";
        
        
        
        return $this->db->query($sql,[$UserID,$UserID]);
    }
    
     public function GroupAddMessage($IDFrom,$GroupID,$MSG,$Time,$Date) {
        
        $sql = "INSERT INTO `messages`( `Message`, `IDFrom`, `GroupID`,`Time`,`Date`) VALUES ('" . $this->db->escape($MSG) . "',{$this->db->escape($IDFrom)},{$this->db->escape($GroupID)},'{$this->db->escape($Time)}','{$this->db->escape($Date)}')";
        
        
        
        $this->db->query($sql);
        
    }
    
    public function AddMessage($IDFrom,$IDTo,$MSG,$Time,$Date) {
        
        $sql = "INSERT INTO `messages`( `Message`, `IDFrom`, `IDTo`,`Time`,`Date`) VALUES ('" . $this->db->escape($MSG) . "',{$this->db->escape($IDFrom)},{$this->db->escape($IDTo)},'{$this->db->escape($Time)}','{$this->db->escape($Date)}')";
        
        
        
        $this->db->query($sql);
        
    }
    
    public function groupRenderMessages($GroupID) {
        $sql = "SELECT * FROM `messages` WHERE `GroupID` = {$this->db->escape($GroupID)} ORDER BY ID DESC LIMIT 25";
        
        return $this->db->query($sql);
    }
    
    public function renderMessages($UserID,$FriendID) {
        $sql = "SELECT * FROM `messages` WHERE `IDFrom` = {$this->db->escape($UserID)} AND `IDTo` = {$this->db->escape($FriendID)} OR `IDTo` = {$this->db->escape($UserID)} AND `IDFrom` = {$this->db->escape($FriendID)} ORDER BY ID DESC LIMIT 25";
        
        return $this->db->query($sql);
    }
    
    public function ControlMessages($ID1,$ID2) {
        $sql = "SELECT count(ID) FROM `messages` WHERE `IDTo` = " . $this->db->escape($ID1) . " AND `IDFrom` =  " . $this->db->escape($ID2) . " AND Readed = 0";
        return $this->db->query($sql);
    }
    
     public function ControlUserMessages($User_ID) {
         
        $sql =  "SELECT count(ID) FROM `messages` WHERE `IDTo` = {$this->db->escape($User_ID)} AND  Readed = 0";
        return $this->db->query($sql);
    }
    
    public function ReadingMessage($ID) {
        $sql = "UPDATE `messages` SET `Readed`= 1 WHERE ID = " . $this->db->escape($ID);
        
        return $this->db->query($sql);
    }
    
     public function EmailSended($ID) {
        $sql = "UPDATE `messages` SET `Esended`= 1 WHERE ID = " . $this->db->escape($ID);
        dd($sql);
        return $this->db->query($sql);
    }
    
   public function selectnotifications() {
        $sql = "SELECT `IDFrom`,`IDTo`,`Message`,`ID` FROM `messages` WHERE `Readed` = 0 AND `Esended` = 0" ;
        
        return $this->db->query($sql);
    } 
    
    public function readnotification($ID) {
         $sql = "UPDATE `messages` SET `BNS`= 1 WHERE `ID` = " . $this->db->escape($ID) ;
        dd($sql);
         $this->db->query($sql); 
    }
    
    public function usernotifications($UserID) {
        $sql = "SELECT  `Message`,CONCAT(users.FirstName,' ', users.LastName) AS `Name`,messages.ID FROM `messages` JOIN `users` ON users.ID = messages.IDFrom  WHERE `Readed` = 0 AND `Esended` = 0 AND `IDTo` = {$this->db->escape($UserID)} AND `BNS` = 0 " ;
        
        return $this->db->query($sql);
    } 
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of newPHPClass
 *
 * @author tomasholub
 */
class UserTokens extends \Library\BaseModel{
    
    public function makeNewToken($Token,$UserID) {
         $sql="INSERT INTO `usertokens`( `Token`, `UserID`)  VALUES('{$this->db->escape($Token)}',{$this->db->escape($UserID)});" ;
        
         $this->db->query($sql);
    }
    
    public function findUserFromToken($Token) {
         $sql="SELECT users.ID FROM `usertokens` JOIN `users` ON usertokens.UserID = users.ID WHERE usertokens.Token = '{$this->db->escape($Token)}'" ;
         
         return $this->db->query($sql);
    }
    
}

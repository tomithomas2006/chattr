<?php

namespace App\Model;

class Movie extends \Library\BaseModel {
    
    /** @var string */
    protected $title;
    
    /** @var \DateTime */
    protected $issueDate;
    
    /** @var int */
    protected $duration;
    
    /** @var string */
    protected $description;
    
    /** @var Person */
    protected $director;
    
    public function setValues($res) {
        $this->setId($res->row['id']);
        
        $this->setTitle($res->row['title']);
        
        $issueDate = new \DateTime($res->row['issue_date']);
        $this->setIssueDate($issueDate);
        
        $this->setDuration($res->row['duration']);
        $this->setDescription($res->row['description']);
        
        $director = $this->container->createPerson();
        $director->find($res->row['director_id']);
        $this->setDirector($director);
    }
    
    public function getTitle() {
        return $this->title;
    }

    public function getIssueDate() {
        return $this->issueDate;
    }

    public function getDuration() {
        return $this->duration;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getDirector() {
        return $this->director;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function setIssueDate(\DateTime $issueDate) {
        $this->issueDate = $issueDate;
        return $this;
    }

    public function setDuration($duration) {
        $this->duration = $duration;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setDirector(Person $director) {
        $this->director = $director;
        return $this;
    }


}

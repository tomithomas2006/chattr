<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;


class Friends extends \Library\BaseModel{
    
    
    
    public function AddNewFriendRequest($idfrom,$idto)
    {
        
        $sql = "INSERT INTO `friends`( `RequestIDFrom`,`RequestIDTo`) VALUES ({$this->db->escape($idfrom)},{$this->db->escape($idto)})";
        dd($sql);
        
        $this->db->query($sql);
        
    }
    
    public function FindUserRequests($UserID) {
       $sql =  "SELECT * FROM `friends` WHERE  `RequestIDTo` = {$this->db->escape($UserID)} AND `Accept` = 0";
       return $this->db->query($sql);
    }
    
    public function ControlUserRequests($id1,$id2)
    {
       $sql = "SELECT * FROM `friends` WHERE `RequestIDFrom` = {$this->db->escape($id1)} AND `RequestIDTo` = {$this->db->escape($id2)} AND `Accept` = 0 OR `Accept` = 0 AND`RequestIDFrom` = {$this->db->escape($id2)} AND `RequestIDTo` = {$this->db->escape($id1)} OR `RequestIDFrom` = {$this->db->escape($id1)} AND `RequestIDTo` = {$this->db->escape($id2)} AND `Accept` = 1 OR `Accept` = 1 AND`RequestIDFrom` = {$this->db->escape($id2)} AND `RequestIDTo` = {$this->db->escape($id1)} "; 
       dd($sql);
       return $this->db->query($sql);
    }
    
    public function AcceptRequest($ID) {
        $sql = "UPDATE `friends` SET `Accept`= 1 WHERE `ID` = {$this->db->escape($ID)}";
        $this->db->query($sql);
    }
    
    public function FindFriends($UserID)
    {
        $sql =  "SELECT * FROM `friends` JOIN users ON users.ID = (CASE
WHEN `RequestIDTo` = {$this->db->escape($UserID)} THEN `RequestIDFrom`
ELSE `RequestIDTo` END) WHERE `Accept` = 1 AND RequestIDFrom = {$this->db->escape($UserID)} OR RequestIDTo = {$this->db->escape($UserID)} ";
        
        return $this->db->query($sql);
    }
    
    public function FindFriendsWithoutJoin($UserID)
    {
        $sql =  "SELECT friends.ID, friends.RequestIDFrom,friends.RequestIDTo,friends.Accept FROM `friends` JOIN users ON users.ID = (CASE
WHEN `RequestIDTo` = {$this->db->escape($UserID)} THEN `RequestIDFrom`
ELSE `RequestIDTo` END) WHERE `Accept` = 1 AND RequestIDFrom = {$this->db->escape($UserID)} OR RequestIDTo = {$this->db->escape($UserID)} ";
        
        return $this->db->query($sql);
    }
    
    public function DeclineRequest($id) {
        $sql = "DELETE FROM `friends` WHERE `ID` = {$this->db->escape($id)}";
        $this->db->query($sql);
    }
    
     public function FindOtherUserFriends($ID,$UserID) {
        $sql = "SELECT * FROM `friends` WHERE `RequestIDFrom` = " . $this->db->escape($ID) . " AND NOT `RequestIDTo` = " . $this->db->escape($UserID) . "OR `RequestIDTo` = " . $this->db->escape($ID) . "AND NOT `RequestIDFrom` = " . $this->db->escape($UserID);
         
    return $this->db->query($sql);
    }
    
    public function FindRecordById($id1,$id2) {
         $sql = "SELECT * FROM `friends` WHERE `RequestIDFrom` = " . $this->db->escape($id1) . " AND `RequestIDTo` = " . $this->db->escape($id2) . " OR `RequestIDFrom` = " . $this->db->escape($id2) . " AND `RequestIDTo` = " . $this->db->escape($id1) ;
         return $this->db->query($sql);
    }
    
    public function FindAcceptedRecordById($id1,$id2) {
         $sql = "SELECT * FROM `friends` WHERE `Accept` = 1 AND  `RequestIDFrom` = " . $this->db->escape($id1) . " AND `RequestIDTo` = " . $this->db->escape($id2) . " OR `Accept` = 1 AND `RequestIDFrom` = " . $this->db->escape($id2) . " AND `RequestIDTo` = " . $this->db->escape($id1) ;
         dd($sql);
         return $this->db->query($sql);
         
    }
    
    public function deleteFriends($User)
    {
        
        $sql = "DELETE FROM `friends` WHERE `IDTo` = {$this->db->escape($User)})";
        dd($sql);
        
        $this->db->query($sql);
        
    }
    
}

<?php



namespace App\Model;


class Posts extends \Library\BaseModel{
    
    public function findWithUserPosts($UserID) {
       $sql = "SELECT * FROM `posts` WHERE UserIDPosts = ? OR UserIDPosted = ?;";
       
       return $this->db->query($sql,[$UserID,$UserID]);
       
    }
    
    public function createPost($UserIDPosts,$PostMSG,$UserIDPosted)
    {
       
        $sql = "INSERT INTO `posts`( `PostMsg`, `UserIDPosted`, `UserIDPosts`,`PostDate`) VALUES ('" . $this->db->escape($PostMSG) . "'," . $this->db->escape($UserIDPosted) . "," . $this->db->escape($UserIDPosts) . ",NOW());";
        
       
        if($this->db->query($sql,[$PostMSG]))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    public function CreateImagePost($UserIDPosts,$PostMSG,$UserIDPosted,$Image) {
       $sql = "INSERT INTO `posts`( `PostMsg`, `UserIDPosted`, `UserIDPosts`,`PostDate`,`Image`) VALUES ('" . $this->db->escape($PostMSG) .  "'," .$this->db->escape($UserIDPosted) . "," . $this->db->escape($UserIDPosts) . ",NOW(),'" . $this->db->escape($Image) . "');";
       
        
        $this->db->query($sql);
    }
    
    public function FLP() {
        $sql = "SELECT  posts.PostMSG,posts.UserIDPosts,posts.PostDate,posts.Image,CONCAT(users.FirstName,' ', users.LastName),posts.ID AS `Name` FROM `posts` JOIN `users` ON users.ID = posts.UserIDPosts  ORDER BY posts.ID DESC LIMIT 10";
       
        dd($sql);
        return $this->db->query($sql);
    }
    
    public function findUserPosts($UserIDPosts) {
       $sql = "SELECT  posts.PostMSG,posts.UserIDPosts,posts.PostDate,posts.Image,CONCAT(users.FirstName,' ', users.LastName) AS `Name`,posts.ID FROM `posts` JOIN `users` ON users.ID = posts.UserIDPosts WHERE UserIDPosts = " . $this->db->escape($UserIDPosts) . " ORDER BY posts.ID DESC;";
       
       return $this->db->query($sql);
       
    }
    
    public function findUserPostsWithImages($UserID)
    {
       $sql = "SELECT  posts.Image FROM `posts` WHERE UserIDPosts = " . $this->db->escape($UserID) . ";";
       
       return $this->db->query($sql); 
    }
}

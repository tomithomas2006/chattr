<?php
namespace App\Model;

class Country extends \Library\BaseModel {
    
    /** @var string */
    protected $code;
    
    /** @var string */
    protected $name;
    
    public function findByCode($code) {
        $sql = "SELECT * FROM {$this->getTableName()} WHERE code = ?;";
        $res = $this->db->query($sql, [$code]);
        $this->setValues($res);
    }
    
    public function setValues($res) {
        $this->setId($res->row['id']);
        $this->setCode($res->row['code']);
        $this->setName($res->row['name']);
    }
    
    public function getCode() {
        return $this->code;
    }

    public function getName() {
        return $this->name;
    }

    public function setCode($code) {
        $this->code = $code;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
}

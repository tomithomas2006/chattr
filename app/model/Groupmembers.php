<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of groupmembers
 *
 * @author tomasholub
 */
class groupmembers extends \Library\BaseModel{
    
      public function findgroupmember($GroupID,$UserID) {
        $sql="SELECT `GroupID` FROM `groupmembers`  WHERE `UserID` = {$this->db->escape($UserID)} AND `GroupID`  = {$this->db->escape($GroupID)} ";
        
        return $this->db->query($sql);
    }
    
    public function findusergroups($UserID) {
        $sql="SELECT name,groups.ID FROM `groupmembers` JOIN `groups` ON groups.ID = groupmembers.GroupID  WHERE groupmembers.UserID = {$this->db->escape($UserID)} ";
        
        return $this->db->query($sql);
    }


    public function addmember($GroupID,$UserID) {
        $sql="INSERT INTO `groupmembers`( `UserID`,`GroupID`) VALUES (" . $this->db->escape($UserID) . "," . $this->db->escape($GroupID) . ")";
        
        $this->db->query($sql);
    }
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of AUsers
 *
 * @author tomit
 */
class activeusers extends \Library\BaseModel{
   
    public function addActiveUser($session,$time,$UserID) {
     $sql="INSERT INTO `activeusers`(`session`, `time`,`UserID`) VALUES ('" . $this->db->escape($session) . "'," . $this->db->escape($time) . ",{$this->db->escape($UserID)})";
     return  $this->db->query($sql);
    }
    
    public function updateActiveUser($time, $session) {
        $sql="UPDATE `activeusers` SET `session`='" . $this->db->escape($session) . "',`time`='" . $time . "' WHERE `session` = " . $this->db->escape($session)   ;
     return  $this->db->query($sql);
    }
    
    public function findspecifiedAU($session) {
        $sql = "SELECT * FROM `activeusers` WHERE `session` = '" . $this->db->escape($session) . "'";
        return $this->db->query($sql);
    }
    
    public function deleteAU() {
        $time = time() - 60;
        $sql = "DELETE FROM `activeusers` WHERE time <  " . $time;
        return  $this->db->query($sql);
    }
    
    public function findAUsers() {
        $sql="SELECT CONCAT(users.FirstName,' ', users.LastName) AS name FROM `activeusers` JOIN `users` ON users.ID = activeusers.UserID";
     return  $this->db->query($sql);
    }
    
}

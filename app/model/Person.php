<?php
namespace App\Model;

class Person extends \Library\BaseModel {
    
    /** @var string */
    protected $firstName;
    
    /** @var string */
    protected $lastName;
    
    /** @var \DateTime */
    protected $birthDate;
    
    /** @var \App\Model\Country */
    protected $country;
    
    /** @var string */
    protected $description;
    
    /** @var string */
    protected $avatar;
    
    public function setValues($res) {
        $this->setId($res->row['id']);
        $this->setFirstName($res->row['first_name']);
        $this->setLastName($res->row['last_name']);
        
        $birthDate = new \DateTime($res->row['birth_date']);
        $this->setBirthDate($birthDate);
        
        $country = $this->container->createCountry();
        $country->find($res->row['country_id']);
        $this->setCountry($country);
        
        $this->setDescription($res->row['description']);
        $this->setAvatar($res->row['avatar']);
    }
   
    public function getFullName() {
        return $this->getFirstName() . " " . $this->getLastName();
    }
    
    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getBirthDate() {
        return $this->birthDate;
    }

    public function getCountry() {
        return $this->country;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getAvatar() {
        return $this->avatar;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    public function setBirthDate(\DateTime $birthDate) {
        $this->birthDate = $birthDate;
        return $this;
    }

    public function setCountry(\App\Model\Country $country) {
        $this->country = $country;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setAvatar($avatar) {
        $this->avatar = $avatar;
        return $this;
    }
    
    public function __toString() {
       return $this->getFullName();
    }


}

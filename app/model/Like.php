<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of Like
 *
 * @author Tom
 */
class Like extends \Library\BaseModel{
   
    public function FindUserLike($UID) {
        $sql="SELECT * FROM `likes` WHERE `UserID` = ?";
        
        return $this->db->query($sql,[$UID]);
    }
    
    public function deleteLike($PID,$UserID) {
        $sql="DELETE FROM `likes` WHERE  `PostID` = " . $this->db->escape($PID) ." AND `UserID` = " . $this->db->escape($UserID);
        dd($sql);
        $this->db->query($sql);
    }
    
    public function findlike($PID,$UID) {
        $sql="SELECT `PostID` FROM `likes` WHERE `PostID` = " . $this->db->escape($PID) . " AND `UserID` = " .$this->db->escape($UID);
        
        return $this->db->query($sql);
    }
    
    public function findpostlikes($PID) {
         $sql="SELECT `PostID` FROM `likes` WHERE `PostID` = " . $this->db->escape($PID) ;
       
        return $this->db->query($sql);
    }
    
    public function addLike($POSTID,$UserID) {
        $sql="INSERT INTO `likes`( `PostID`, `UserID`) VALUES (" . $this->db->escape($POSTID) . "," . $this->db->escape($UserID) . ")";
        dd($sql);
        $this->db->query($sql);
    }
    
}

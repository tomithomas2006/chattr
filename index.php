<?php
 header('Access-Control-Allow-Origin: *');
function __autoload($class) {

    $pathUpperCase = str_replace('\\', '/', $class);
    $pathArray = explode("/", $pathUpperCase);
    $className = array_pop($pathArray) . '.php';

    foreach ($pathArray as &$value) {
        $value = strtolower($value);
    }

    $path = implode("/", $pathArray);

    if (file_exists($path . "/" . $className)) {
        require $path . "/" . $className;

        return true;
    } else {
        return false;
    }
}

/**
 * Zapne Tracy Debugger
 */
function initTracy() {
    if (is_file('tracy/tracy.php')) {
        require 'tracy/tracy.php';
        \Tracy\Debugger::enable(\Tracy\Debugger::DEVELOPMENT);
    }
}

/**
 * @param mixed $var Vypisovana data
 * @param int $depth Maximalni hloubka vypisu
 * @param int $length Maximalni delka vypisovaneho stringu
 */
function dd($var, $depth = 5, $length = 250) {
    \Tracy\Debugger::$maxDepth = $depth;
    \Tracy\Debugger::$maxLength = $length;
    \Tracy\Debugger::barDump($var);
}

require_once 'library/exceptions.php';


$container = new \Library\Container($config);

initTracy();
Tracy\Debugger::enable(['46.254.70.33','90.176.179.209']);

require_once 'library/router.php';



